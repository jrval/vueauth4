require('./bootstrap');

import "bootstrap/dist/css/bootstrap.min.css"

import {createApp} from 'vue';

import routes from './routes';

import App from './App.vue';
import {createPinia} from 'pinia'


import axios from './services/axios'
import auth from "./services/auth";
import VueAxios from "vue-axios";
import PermissionsMixin from './mixins/Permissions'
import RolesMixin from './mixins/Roles'
import {Skeletor} from 'vue-skeletor';

import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

import Datepicker from '@vuepic/vue-datepicker';
import '@vuepic/vue-datepicker/dist/main.css'

/* import the fontawesome core */
import {library} from '@fortawesome/fontawesome-svg-core'
/* import specific icons */
import {faPlus, faPen, faTrashCan, faEye, faCartPlus,faCartShopping,faBasketShopping} from '@fortawesome/free-solid-svg-icons'

// import { faFloppyDisk,faSquareCheck } from '@fortawesome/free-regular-svg-icons'
/* import font awesome icon component */
import {FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText} from '@fortawesome/vue-fontawesome'
/* add icons to the library */
library.add(faPlus, faPen, faTrashCan, faEye, faCartPlus,faCartShopping,faBasketShopping)

const options = {
    customClass: {
        confirmButton: 'm-1 btn btn-success ',
        cancelButton: 'm-1  btn btn-danger'
    },
    buttonsStyling: false
};

const app = createApp(App)
    .use(createPinia())
    .use(auth)
    .use(routes)
    .use(VueAxios, axios)
    .use(VueSweetalert2, options)
app.mixin(RolesMixin);
app.mixin(PermissionsMixin);

app.component('Datepicker', Datepicker);
app.component(Skeletor.name, Skeletor);
app.component('font-awesome-icon', FontAwesomeIcon);
app.component('font-awesome-layers', FontAwesomeLayers);
app.component('font-awesome-layers-text', FontAwesomeLayersText);

app.mount('#app');

import "bootstrap/dist/js/bootstrap.js"
