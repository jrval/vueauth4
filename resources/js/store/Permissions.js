import { defineStore } from "pinia";
import axios from "axios";

export const usePermissionStore = defineStore("permissions", {
    state: () => ({
        permissions:[],
        permission:{},
        permissionData:{},
        isLoading:false
    }),

    getters: {},

    actions: {
        async allPermissions() {
            const uri = '/api/permissions';
            const response = await axios.get(uri)
            this.permissions = response.data
        },
        async fetchPermissionForDatatable(options, page) {
            this.isLoading=true
            const uri = '/api/permissions/datatable?page=' + page + '&search=' + options.search + '&sortby=' + options.currentSort + '&sortdir=' + options.currentSortDir + '&currentpage=' + options.currentPage;
            const response = await axios.get(uri)
            this.permissionData = response.data
            this.isLoading=false
        },
        async fetchPermission(id) {

            const {data} = (await axios.get(`api/permissions/${id}`));
            this.permission = data;
        },

        async updatePermission(id) {
            await axios.patch(`api/permissions/${id}`, this.permission);
            this.permission = {};
        },
        async createPermission() {
            await axios.post(`api/permissions`, this.permission);
            this.permission = {};
        },
        async removePermission(id) {
            let index = this.permissionData.data.findIndex((details) => details.id === id);
            await axios.delete(`api/permissions/${id}`);
            this.permissionData.data.splice(index, 1);
        },
    },
});
