import {defineStore} from "pinia";

import moment from 'moment';


export const useSelectedDate = defineStore("SelectedDate", {
    state: () => ({
        today: moment(),
        date_range: [
            moment().startOf('isoWeek').add(1, 'week').format('yyyy-MM-DD').toString(),
            moment().endOf('isoWeek').add(1, 'week').format('yyyy-MM-DD').toString()
        ],
    }),
    getters: {
        from_date: (state) => state.today.startOf('isoWeek').toString(),
        to_date: (state) => state.today.endOf('isoWeek').toString(),
    },

    actions: {},
});
