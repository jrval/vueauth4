import {defineStore} from "pinia";
import axios from "axios";
import moment from 'moment';
import jsonToFormData from "../services/jsonToFormData";

export const useFoodMenu = defineStore("FoodMenu", {
    state: () => ({
        form_data: {
            date_selected: null,
            name: null,
            ingredients: null,
            allergens: null,
            price: 0,
            image_upload: {},
            is_date_valid: null,

        },
        food_menus: [],


    }),
    getters: {
        disableDates: (state) => {
            return state.food_menus.map(i => new Date(i.date));
        },

    },

    actions: {
        async getFoodMenuFilterByDate(date_range) {

            let start_date = moment(date_range[0]).format('yyyy-MM-DD');
            let end_date = moment(date_range[1]).format('yyyy-MM-DD');

            const {data} = await axios.get(`/api/food-menu/${start_date}/${end_date}/filter-date`)

            this.food_menus = data;
        },
        async createFoodMenu(config) {
            this.form_data.date_selected = moment(this.form_data.date_selected).format('yyyy-MM-DD');
            let data = jsonToFormData(this.form_data)
            await axios.post(`api/food-menu`, data, {config});
            this.form_data = {
                date_selected: new Date(data.date),
                name: null,
                ingredients: null,
                allergens: null,
                price: 0,
                image_upload: {},
                id: null,
                is_date_valid: null
            }
        },

        async show(id) {
            const {data} = await axios.get(`/api/food-menu/${id}`)
            this.form_data = {
                date_selected: data.date,
                name: data.name,
                ingredients: data.ingredients,
                allergens: data.allergens,
                price: data.price,
                image_upload: data.image_details,
                id: Number(id),
                is_date_valid: data.is_date_valid
            }
        },
        async updateFoodMenu(id, config) {
            this.form_data.date_selected = moment(this.form_data.date_selected).format('yyyy-MM-DD');
            let form_data = jsonToFormData(this.form_data)
            form_data.append('_method', 'PATCH');
            const {data} = await axios.post(`api/food-menu/${id}`, form_data, {config});
            this.form_data = data;
        },

        async removeFoodMenu(id) {
            return await axios.delete(`api/food-menu/${id}`);
        }

    },
});
