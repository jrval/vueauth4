import {defineStore} from "pinia";
import axios from "axios";

export const useDashboardStore = defineStore("dashboard", {
    state: () => ({
        todays_food: {},
        order_count: 0,
        claimed_count: 0
    }),

    getters: {},

    actions: {
        async getTodaysMenu() {

            const uri = '/api/dashboard/todays-food';
            const response = await axios.get(uri)
            this.todays_food = response.data

        },

        async orderCount() {

            const uri = '/api/dashboard/order-count';
            const response = await axios.get(uri)
            this.order_count = response.data

        },

        async claimedCount() {

            const uri = '/api/dashboard/claimed-count';
            const response = await axios.get(uri)
            this.claimed_count = response.data
        },
    },
});
