import {defineStore} from "pinia";
import axios from "axios";


export const useVmcSubsidyStore = defineStore("vmc_subsidy", {
    state: () => ({
        subsidy: {}
    }),

    getters: {},

    actions: {
        async getSubsidy() {

            const {data} = (await axios.get(`api/vmc-subsidies/get-subsidy`));
            this.subsidy = data;
        },
    },
});

