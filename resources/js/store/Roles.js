import {defineStore} from "pinia";
import axios from "axios";

export const useRoleStore = defineStore("roles", {
    state: () => ({
        roles: [],
        roleData: {},
        role: {
            permissions:[]
        },
        isLoading:true
    }),

    getters: {},

    actions: {
        async allRoles() {
            const uri = '/api/roles';
            const response = await axios.get(uri)
            this.roles = response.data
        },
        async fetchRoleForDatatable(options, page) {
            this.isLoading=true
            const uri = '/api/roles/datatable?page=' + page + '&search=' + options.search + '&sortby=' + options.currentSort + '&sortdir=' + options.currentSortDir + '&currentpage=' + options.currentPage;
            const response = await axios.get(uri)
            this.roleData = response.data
            this.isLoading=false

        },
        async fetchRole(id) {

            const {data} = (await axios.get(`api/roles/${id}`));
            this.role = data;
        },
        async updateRole(id) {
            await axios.patch(`api/roles/${id}`, this.role);
            this.role = {};
        },
        async createRole() {
            await axios.post(`api/roles`, this.role);
            this.role = {};
        },
        async removeRole(id) {
            let index = this.roleData.data.findIndex((details) => details.id === id);
            await axios.delete(`api/roles/${id}`);
            this.roleData.data.splice(index, 1);
        },

    },
});
