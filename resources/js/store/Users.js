import { defineStore } from "pinia";
import axios from "axios";

export const useUsersStore = defineStore("users", {
    state: () => ({
        users:[],
        user:{
            roles:[]
        },
        userData:{},
        isLoading:false
    }),

    getters: {},

    actions: {

        async fetchUsersForDatatable(options, page) {
            this.isLoading=true
            const uri = '/api/users/datatable?page=' + page + '&search=' + options.search + '&sortby=' + options.currentSort + '&sortdir=' + options.currentSortDir + '&currentpage=' + options.currentPage;
            const response = await axios.get(uri)
            this.userData = response.data
            this.isLoading=false
        },
        async fetchUser(id) {

            const {data} = (await axios.get(`api/users/${id}`));
            this.user = data;
        },

        async updateUser(id) {
            await axios.patch(`api/users/${id}`, this.user);
            this.permission = {};
        },
        async createUser() {
            await axios.post(`api/users`, this.user);
            this.permission = {};
        },
        async removeUser(id) {
            let index = this.userData.data.findIndex((details) => details.id === id);
            await axios.delete(`api/users/${id}`);
            this.userData.data.splice(index, 1);
        },
    },
});

