import {defineStore} from "pinia";
import axios from "axios";


export const useCart = defineStore("Cart", {
    state: () => ({
        quantity: 1,
        cart: {},
        cart_by_id: {},
        cart_count: 0,
        cart_list: [],
        isLoading: {
            add_to_cart: false
        },
        claimed_food: {
            status: null
        }
    }),
    getters: {
        total_price() {
            return this.valid_date_cart.reduce(function (total, item) {
                return total + (Number(item.food_menu.price) * Number(item.quantity))
            }, 0);
        },
        valid_date_cart: (state) => {
            return state.cart_list.filter((e) => {
                return e.food_menu.is_date_valid
            })
        }
    },

    actions: {
        async getCart(payload) {
            this.claimed_food = {
                status: null
            }
            const {data} = await axios.get(`api/cart/${payload.instance}`); // get cart
            this.cart = data
        },
        async claimFood(username) {
            this.claimed_food = {
                status: null
            }
            const {data} = await axios.get(`api/cart/${username}/claim-food`); // get cart
            if (Object.keys(data).length) {
                this.claimed_food = {...data}
                this.claimed_food.status = 'not-empty'
            } else {
                this.claimed_food = {...data}
                this.claimed_food.status = 'empty'
            }

        },
        async getCartById(id) {
            const {data} = await axios.get(`api/cart/${id}/cart-id`); // get cart
            this.cart_by_id = data
        },
        async addToCart(payload) {
            this.isLoading.add_to_cart = true
            const {data} = await axios.get(`api/cart/${payload.instance}`); // get cart
            this.cart = data
            let cart_food_menu = {
                'cart_id': this.cart.id,
                'menu_id': payload.menu_id,
                'quantity': this.quantity
            }
            await axios.post('api/cart-food-menu', cart_food_menu)
            await this.getFoodCart(payload.instance)
            this.isLoading.add_to_cart = false

        },
        async addQty(cart, instance) {
            await axios.put(`api/cart-food-menu/${cart.id}`, cart); // get cart
            await this.getFoodCart(instance)
        },

        // async countCart(instance) {
        //     const {data} = await axios.get(`api/cart/${instance}/count`); // get cart
        //     this.cart_count = data
        // },

        async getFoodCart(instance) {
            const {data} = await axios.get(`api/cart-food-menu`, {
                params: {
                    instance: instance
                }
            }); // get cart
            this.cart_list = data
        },
        // async deleteFoodMenu(id) {
        //     const {data} = await axios.delete(`api/cart-food-menu`, {
        //         params: {
        //             instance: instance
        //         }
        //     }); // get cart
        //
        //     const {data} = await axios.get(`api/cart/${payload.instance}`); // get cart
        //     this.cart_list = data
        // },
        async deleteFoodMenu(id) {
            let index = this.cart_list.findIndex((cart_list) => cart_list.id === id);
            await axios.delete(`api/cart-food-menu/${id}`);
            await this.cart_list.splice(index, 1);
        },

    },
});
