import {defineStore} from "pinia";
import axios from "axios";
import moment from "moment/moment";


export const useOrder = defineStore("order", {
    state: () => ({
        isLoading: {
            submit_order: false,
            order_list: false
        },
        orders: {},
        ordersData: {}
    }),

    getters: {},

    actions: {
        async submitOrder(inputs, subsidy_decimal, net_total, total_price) {
            this.isLoading.submit_order = true
            await axios.post(`api/orders`, {
                valid_date_cart: JSON.stringify(inputs),
                subsidy_decimal: subsidy_decimal,
                net_total: net_total,
                total_price: total_price
            });
            this.isLoading.submit_order = false
        },
        async getOrders(options, page) {

            const uri = '/api/orders/datatable?page=' + page + '&currentpage=' + options.currentPage;
            const response = await axios.get(uri)
            this.orders = response.data

        },

        async fetchOrdersAll(options, page, date_range) {
            let start_date = moment(date_range[0]).format('yyyy-MM-DD');
            let end_date = moment(date_range[1]).format('yyyy-MM-DD');
            this.isLoading.order_list = true
            const uri = '/api/orders/order-list-all?page=' + page + '&search=' + options.search + '&sortby=' + options.currentSort + '&sortdir=' + options.currentSortDir + '&currentpage=' + options.currentPage + '&start_date=' + start_date + '&end_date=' + end_date + '&is_claimed=' + options.is_claimed;
            const response = await axios.get(uri)
            this.isLoading.order_list = false
            this.ordersData = response.data

        },
    },
});
