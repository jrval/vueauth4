import { useAuthStore } from "../store/Authentication";

export default {
    install: ({ config }) => {
        config.globalProperties.$auth = useAuthStore();

        if (useAuthStore().loggedIn) {
            useAuthStore().fetchUser();
            useAuthStore().fetchAbilities();
        }
    },
};
