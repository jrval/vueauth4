import {createWebHistory, createRouter} from "vue-router";
import routes from "./routes";
import {useAuthStore} from "../store/Authentication";
import {pageTitle} from "../store/PageTitle";


const router = createRouter({
    history: createWebHistory(),
    routes,
    linkActiveClass: "active",
    linkExactActiveClass: "active",
});


router.beforeEach(async (to, from, next) => {
    document.title = to.meta.title + ' | ' + process.env.MIX_APP_NAME;
    pageTitle().pageName = to.meta.title;

    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!useAuthStore().loggedIn) {

            next({
                path: '/login',
                query: {redirect: to.fullPath}
            })

        } else {

            const permissions = (typeof useAuthStore().permissions == "object" ? useAuthStore().permissions : JSON.parse(useAuthStore().permissions)) || [];
            const roles = (typeof useAuthStore().roles == "object" ? useAuthStore().roles : JSON.parse(useAuthStore().roles)) || [];
            const authStore = useAuthStore()
            authStore.fetchUser();
            authStore.fetchAbilities();

            if (roles.includes('super_administrator') || roles.includes('food_maintenance_admin')) {
                //super_administrator
                return next();

            } else if (!to.meta.roles) {
                //if no role present

                if (!to.meta.permissions) {
                    return next();
                } else {
                    if (permissions.some(r => to.meta.permissions.includes(r))) {
                        next();
                    } else {
                        next('/404');
                    }
                }

            } else {

                if (roles.some(r => to.meta.roles.includes(r))) {
                    next();
                } else {
                    next('/404');
                }
            }
        }
    } else {

        if (to.path === '/login' && useAuthStore().loggedIn) next({name: 'home',});
        else next()

    }
});


export default router;
