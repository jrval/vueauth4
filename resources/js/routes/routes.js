let Main = () => import(/* webpackChunkName: "main" */'./../components/Main');

let Login = () => import(/* webpackChunkName: "login" */'../pages/authentication/login');

let HomePage = () => import(/* webpackChunkName: "HomePage" */'./../pages/homePage');

//ROLES
let IndexRole = () => import(/* webpackChunkName: "Roles" */'./../pages/roles/indexRole');
let TableRole = () => import(/* webpackChunkName: "Roles" */'./../pages/roles/tableRole');

//PERMISSIONS
let IndexPermission = () => import(/* webpackChunkName: "Permissions" */'./../pages/permissions/indexPermission');
let TablePermission = () => import(/* webpackChunkName: "Permissions" */'./../pages/permissions/tablePermission');

//USERS
let IndexUser = () => import(/* webpackChunkName: "Users" */'./../pages/users/indexUser');
let TableUser = () => import(/* webpackChunkName: "Users" */'./../pages/users/tableUser');


//Reservation
let IndexReservation = () => import(/* webpackChunkName: "IndexReservation" */'./../pages/reservation/indexReservation');
let Reservation = () => import(/* webpackChunkName: "Reservation" */'./../pages/reservation/reservations');
let Cart = () => import(/* webpackChunkName: "Cart" */'./../pages/reservation/cartView');
let CheckOut = () => import(/* webpackChunkName: "CheckOut" */'./../pages/reservation/checkOut');

//Food Maintenance
let IndexFoodMaintenance = () => import(/* webpackChunkName: "FoodMaintenance" */'./../pages/food-maintenance/indexFoodMaintenance');
let CreateFoodMaintenance = () => import(/* webpackChunkName: "FoodMaintenance" */'./../pages/food-maintenance/foodCreate');

//MyOrders
let IndexMyOrder = () => import(/* webpackChunkName: "Order" */'../pages/my-orders/orderIndex');
let MyOrder = () => import(/* webpackChunkName: "Order" */'../pages/my-orders/orderView');

//ScanQr
let IndexScanQR = () => import(/* webpackChunkName: "ScanQR" */'./../pages/ScanQR/ScanQRIndex');
let ScanQr = () => import(/* webpackChunkName: "ScanQR" */'./../pages/ScanQR/ScanQr');

let Error_404 = () => import(/* webpackChunkName: "Error404" */'./../pages/errors/error_404');


//Orders
let IndexOrder = () => import(/* webpackChunkName: "Order" */'../pages/orders/orderIndex');
let OrderList = () => import(/* webpackChunkName: "Order" */'../pages/orders/orderList');

const routes = [
    {
        path: "/login",
        name: "login",
        component: Login,
        meta: {
            title: 'Login',
            requiresAuth: false,
        },
    },
    {
        path: '/',
        redirect: {name: 'home'},
        component: Main,
        children: [
            {
                path: '/home',
                component: HomePage,
                name: 'home',
                meta: {
                    title: 'Home',
                    requiresAuth: true,
                },
            },
            {
                path: '/roles',
                component: IndexRole,
                name: 'roleIndex',
                children: [
                    {
                        path: "",
                        component: TableRole,
                        name: "TableRole",
                        meta: {
                            title: 'Roles',
                            requiresAuth: true,
                            permissions: ['read_role'],
                        },
                    }
                ]
            },
            {
                path: '/permissions',
                component: IndexPermission,
                name: 'permissionIndex',
                children: [
                    {
                        path: "",
                        component: TablePermission,
                        name: "TablePermission",
                        meta: {
                            title: 'Permissions',
                            requiresAuth: true,
                            permissions: ['read_permission'],
                        },
                    }
                ]
            },
            {
                path: '/users',
                component: IndexUser,
                name: 'userIndex',
                children: [
                    {
                        path: "",
                        component: TableUser,
                        name: "TableUser",
                        meta: {
                            title: 'Users',
                            requiresAuth: true,
                            permissions: ['user_view'],
                        },
                    }
                ]
            },

            {
                path: '/reservations',
                component: IndexReservation,
                name: 'reservationIndex',
                redirect: {name: 'ReservationView'},

                children: [
                    {
                        path: "",
                        component: Reservation,
                        name: "ReservationView",
                        meta: {
                            title: 'Reservations',
                            requiresAuth: true,
                            //permissions: ['reservation_view'],
                        },
                    },
                    {
                        path: "basket",
                        component: Cart,
                        name: "CartView",
                        meta: {
                            title: 'Basket',
                            requiresAuth: true,
                            //permissions: ['cart_view'],
                        },
                    },
                    {
                        path: "check-out",
                        component: CheckOut,
                        name: "CheckOut",
                        meta: {
                            title: 'Check-out',
                            requiresAuth: true,
                            //permissions: ['cart_check_out'],
                        },
                    }
                ]
            },
            {
                path: '/food-maintenance',
                component: IndexFoodMaintenance,
                name: 'foodMaintenanceIndex',
                meta: {
                    title: 'Food Maintenance',
                    requiresAuth: true,
                    permissions: ['food_maintenance_view'],
                },
                // children: [
                //     {
                //         path: "create",
                //         component: CreateFoodMaintenance,
                //         name: "foodMaintenanceCreate",
                //         props:true,
                //         meta: {
                //             title: 'Food Maintenance Create',
                //             requiresAuth: true,
                //             showModal:true
                //         },
                //     }
                // ]
            },
            {
                path: '/my-orders',
                component: IndexMyOrder,
                name: 'orderIndex',
                redirect: {name: 'OrdersView'},
                children: [
                    {
                        path: "",
                        component: MyOrder,
                        name: "OrdersView",
                        meta: {
                            title: 'My Orders',
                            requiresAuth: true,
                            //permissions: ['order_view'],
                        },
                    },
                ]

            },
            {
                path: '/orders',
                component: IndexOrder,
                name: 'orderMainIndex',
                redirect: {name: 'OrdersMainList'},
                children: [
                    {
                        path: "",
                        component: OrderList,
                        name: "OrdersMainList",
                        meta: {
                            title: 'Orders',
                            requiresAuth: true,
                            permissions: ['order_main_view'],
                        },
                    },
                ]

            },

            {
                path: '/scan-qr',
                component: IndexScanQR,
                name: 'ScanQrIndex',
                redirect: {name: 'ScanQr'},
                children: [
                    {
                        path: "",
                        component: ScanQr,
                        name: "ScanQr",
                        meta: {
                            title: 'Scan QR',
                            requiresAuth: true,
                            permissions: ['scan_qr'],
                        },
                    },
                ]

            },
        ]
    },
    {
        path: "/:pathMatch(.*)",
        component: Error_404,
        meta: {
            requiresAuth: false,
        }
    },
]


export default routes;

