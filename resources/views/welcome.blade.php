<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Laravel 8 Vue 3 Boilerplate</title>

        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    </head>
    <body class="sb-nav-fixed" id="app">

    </body>

    <!-- Vuejs -->
    <script src="{{ mix('js/app.js') }}"></script>

</html>
