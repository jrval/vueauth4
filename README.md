
It's a pre-configured project using Laravel 8, Vue 3, Vue router, Pinia and Bootstrap 5

to get it up and run, just clone it and enjoy coding :

     git clone this project

     cd folder



then run the following commands 

     composer install


     npm install


     cp .env.example .env


go to .env and setup your database and then run the following commands



     php artisan migrate


     php artisan key:generate

finally create your dream wep app
