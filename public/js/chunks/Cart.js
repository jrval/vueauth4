"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["Cart"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/pages/reservation/cartView.vue?vue&type=script&setup=true&lang=js":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/pages/reservation/cartView.vue?vue&type=script&setup=true&lang=js ***!
  \********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _store_Cart__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../store/Cart */ "./resources/js/store/Cart.js");
/* harmony import */ var pinia__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! pinia */ "./node_modules/pinia/dist/pinia.esm-browser.js");
/* harmony import */ var _store_Error__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../store/Error */ "./resources/js/store/Error.js");
/* harmony import */ var _store_VmcSubsidies__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../store/VmcSubsidies */ "./resources/js/store/VmcSubsidies.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }






/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  setup: function setup(__props, _ref) {
    var expose = _ref.expose;
    expose();
    var vmc_subsidy = (0,_store_VmcSubsidies__WEBPACK_IMPORTED_MODULE_4__.useVmcSubsidyStore)();
    var cartStore = (0,_store_Cart__WEBPACK_IMPORTED_MODULE_2__.useCart)();

    var _storeToRefs = (0,pinia__WEBPACK_IMPORTED_MODULE_5__.storeToRefs)(cartStore),
        cart_list = _storeToRefs.cart_list;

    var _storeToRefs2 = (0,pinia__WEBPACK_IMPORTED_MODULE_5__.storeToRefs)(vmc_subsidy),
        subsidy = _storeToRefs2.subsidy;

    var getFoodCart = cartStore.getFoodCart,
        deleteFoodMenu = cartStore.deleteFoodMenu,
        addQty = cartStore.addQty;
    var getSubsidy = vmc_subsidy.getSubsidy;
    var swal = (0,vue__WEBPACK_IMPORTED_MODULE_1__.inject)('$swal');
    var error = (0,_store_Error__WEBPACK_IMPORTED_MODULE_3__.useErrorStore)();
    var subsidy_amount = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)();
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.onMounted)(function () {
      getFoodCart('adding');
      getSubsidy();
    });
    var cartMenuComputed = (0,vue__WEBPACK_IMPORTED_MODULE_1__.reactive)({
      total_price: (0,vue__WEBPACK_IMPORTED_MODULE_1__.computed)(function () {
        return (0,_store_Cart__WEBPACK_IMPORTED_MODULE_2__.useCart)().total_price;
      }),
      valid_cart: (0,vue__WEBPACK_IMPORTED_MODULE_1__.computed)(function () {
        return (0,_store_Cart__WEBPACK_IMPORTED_MODULE_2__.useCart)().valid_date_cart;
      })
    }); // watch(
    //     () => useCart().valid_date_cart,
    //     async (newValue) => {
    //         console.log(newValue);
    //     }, {deep: true}
    // )

    var cartQuantity = _.debounce(function (cart, instance) {
      addQty(cart, instance);
    }, 100);

    var netTotal = function netTotal(total) {
      return total - subsidy_amount.value;
    };

    var subsidyAmount = function subsidyAmount(total, percentage) {
      var subsidy = parseFloat(percentage) / 100;
      subsidy_amount.value = total * subsidy;
      return subsidy_amount.value;
    };

    var removeFoodMenu = /*#__PURE__*/function () {
      var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee2(id) {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                swal({
                  icon: 'error',
                  title: 'Do you want to delete this menu?',
                  showCancelButton: true,
                  confirmButtonText: 'Yes'
                }).then( /*#__PURE__*/function () {
                  var _ref3 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee(result) {
                    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
                      while (1) {
                        switch (_context.prev = _context.next) {
                          case 0:
                            _context.next = 2;
                            return result.isConfirmed;

                          case 2:
                            if (!_context.sent) {
                              _context.next = 7;
                              break;
                            }

                            _context.next = 5;
                            return deleteFoodMenu(id);

                          case 5:
                            _context.next = 7;
                            return swal({
                              icon: 'success',
                              title: 'Deleted',
                              text: 'successfully deleted',
                              showConfirmButton: false,
                              timer: 800
                            });

                          case 7:
                          case "end":
                            return _context.stop();
                        }
                      }
                    }, _callee);
                  }));

                  return function (_x2) {
                    return _ref3.apply(this, arguments);
                  };
                }());

              case 1:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      return function removeFoodMenu(_x) {
        return _ref2.apply(this, arguments);
      };
    }();

    var __returned__ = {
      vmc_subsidy: vmc_subsidy,
      cartStore: cartStore,
      cart_list: cart_list,
      subsidy: subsidy,
      getFoodCart: getFoodCart,
      deleteFoodMenu: deleteFoodMenu,
      addQty: addQty,
      getSubsidy: getSubsidy,
      swal: swal,
      error: error,
      subsidy_amount: subsidy_amount,
      cartMenuComputed: cartMenuComputed,
      cartQuantity: cartQuantity,
      netTotal: netTotal,
      subsidyAmount: subsidyAmount,
      removeFoodMenu: removeFoodMenu,
      computed: vue__WEBPACK_IMPORTED_MODULE_1__.computed,
      inject: vue__WEBPACK_IMPORTED_MODULE_1__.inject,
      onMounted: vue__WEBPACK_IMPORTED_MODULE_1__.onMounted,
      reactive: vue__WEBPACK_IMPORTED_MODULE_1__.reactive,
      ref: vue__WEBPACK_IMPORTED_MODULE_1__.ref,
      useCart: _store_Cart__WEBPACK_IMPORTED_MODULE_2__.useCart,
      storeToRefs: pinia__WEBPACK_IMPORTED_MODULE_5__.storeToRefs,
      useErrorStore: _store_Error__WEBPACK_IMPORTED_MODULE_3__.useErrorStore,
      useVmcSubsidyStore: _store_VmcSubsidies__WEBPACK_IMPORTED_MODULE_4__.useVmcSubsidyStore
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/pages/reservation/cartView.vue?vue&type=template&id=133bc49e":
/*!*************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/pages/reservation/cartView.vue?vue&type=template&id=133bc49e ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render)
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

var _hoisted_1 = {
  "class": "container px-3 my-5 clearfix"
};
var _hoisted_2 = {
  "class": "card"
};
var _hoisted_3 = {
  "class": "card-body"
};
var _hoisted_4 = {
  key: 0,
  "class": "alert alert-danger",
  role: "alert"
};
var _hoisted_5 = {
  "class": "table-responsive"
};
var _hoisted_6 = {
  "class": "table"
};

var _hoisted_7 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("thead", null, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("tr", null, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("th", null, "#"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("th", null, "Food Menu"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                            <th>Price</th>"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("th", null, "Quantity"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                            <th>Total</th>"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("th")])], -1
/* HOISTED */
);

var _hoisted_8 = {
  "class": "card mb-3",
  style: {
    "max-width": "540px"
  }
};
var _hoisted_9 = {
  "class": "row g-0"
};
var _hoisted_10 = {
  "class": "col-md-4"
};
var _hoisted_11 = ["src"];
var _hoisted_12 = {
  key: 1,
  alt: "",
  "class": "img-fluid rounded-start",
  src: "http://placehold.jp/3d4070/ffffff/.png?text=no%20image",
  style: {
    "width": "300px"
  }
};
var _hoisted_13 = {
  "class": "col-md-8"
};
var _hoisted_14 = {
  "class": "card-body"
};
var _hoisted_15 = {
  "class": "card-title"
};
var _hoisted_16 = {
  "class": "card-text h6"
};
var _hoisted_17 = {
  "class": "card-text"
};
var _hoisted_18 = ["onClick"];
var _hoisted_19 = {
  key: 1
};

var _hoisted_20 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", {
  "class": "text-center",
  colspan: "4"
}, " No Data ", -1
/* HOISTED */
);

var _hoisted_21 = [_hoisted_20];
var _hoisted_22 = {
  "class": "card-footer text-end text-nowrap"
};

var _hoisted_23 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Back to Food Menu List ");

var _hoisted_24 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Check-out ");

function render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_router_link = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("router-link");

  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [$setup.error.errors.quantity ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_4, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.error.errors.quantity.toString()), 1
  /* TEXT */
  )) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("table", _hoisted_6, [_hoisted_7, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("tbody", null, [$setup.cartMenuComputed && $setup.cartMenuComputed.valid_cart.length ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    key: 0
  }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.cartMenuComputed.valid_cart, function (cart, index) {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("tr", {
      key: cart.id
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(index + 1), 1
    /* TEXT */
    ), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_8, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_9, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_10, [cart.food_menu.image_details && cart.food_menu.image_details.filename ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
      key: 0,
      src: '/' + cart.food_menu.image_details.filename,
      alt: "",
      "class": "img-fluid rounded-start",
      style: {
        "width": "300px"
      }
    }, null, 8
    /* PROPS */
    , _hoisted_11)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", _hoisted_12))]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_13, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_14, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h5", _hoisted_15, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(cart.food_menu.date), 1
    /* TEXT */
    ), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", _hoisted_16, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(cart.food_menu.name), 1
    /* TEXT */
    ), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", _hoisted_17, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(cart.food_menu.ingredients), 1
    /* TEXT */
    )])])])])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                            <td>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                {{ cart.food_menu.price }}"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                            </td>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)((0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(cart.quantity), 1
    /* TEXT */
    ), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                <input v-model.number=\"cart.quantity\""), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                       class=\"form-control text-end\""), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                       min=\"1\" step=\"1\" type=\"number\""), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                       @change=\"cartQuantity(cart,'adding')\">")]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                            <td>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                {{ cart.food_menu.price * cart.quantity }}"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                            </td>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("button", {
      "class": "btn btn-danger btn-sm",
      onClick: function onClick($event) {
        return $setup.removeFoodMenu(cart.id);
      }
    }, "X", 8
    /* PROPS */
    , _hoisted_18)])]);
  }), 128
  /* KEYED_FRAGMENT */
  )) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("tr", _hoisted_19, _hoisted_21))]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                        <tfoot>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                        <tr>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                            <td colspan=\"4\"></td>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                            <td class=\"text-nowrap\">"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                <label class=\"text-muted font-weight-normal m-0\">Total</label>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                <div class=\"text-large text-end\"><h4>{{"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                        cartMenuComputed.total_price"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                    }}</h4><strong>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                </strong></div>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                            </td>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                        </tr>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                        <tr>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                            <td colspan=\"4\"></td>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                            <td class=\"text-nowrap\">"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                <label class=\"text-muted font-weight-normal m-0\">VMC Subsidy({{"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                        subsidy.percentage"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                    }}% off)</label>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                <div class=\"text-large text-end\"><h5>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                    -{{ subsidyAmount(cartMenuComputed.total_price, subsidy.percentage) }}</h5><strong>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                </strong></div>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                            </td>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                        </tr>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                        <tr>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                            <td colspan=\"4\"></td>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                            <td class=\"text-nowrap\">"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                <label class=\"text-muted font-weight-normal m-0\">Total Net</label>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                <div class=\"text-large text-end text-success\"><h2>P {{"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                        netTotal(cartMenuComputed.total_price)"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                    }}</h2><strong>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                                </strong></div>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                            </td>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                        </tr>"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("                        </tfoot>")])])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_22, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_router_link, {
    to: {
      name: 'ReservationView'
    },
    "class": "btn btn-link",
    exact: "",
    tag: "button"
  }, {
    "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
      return [_hoisted_23];
    }),
    _: 1
    /* STABLE */

  }), $setup.cartMenuComputed && $setup.cartMenuComputed.valid_cart.length ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_router_link, {
    key: 0,
    to: {
      name: 'CheckOut'
    },
    "class": "btn btn-info",
    exact: "",
    tag: "button"
  }, {
    "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
      return [_hoisted_24];
    }),
    _: 1
    /* STABLE */

  })) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])])]);
}

/***/ }),

/***/ "./resources/js/store/Cart.js":
/*!************************************!*\
  !*** ./resources/js/store/Cart.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "useCart": () => (/* binding */ useCart)
/* harmony export */ });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var pinia__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! pinia */ "./node_modules/pinia/dist/pinia.esm-browser.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



var useCart = (0,pinia__WEBPACK_IMPORTED_MODULE_2__.defineStore)("Cart", {
  state: function state() {
    return {
      quantity: 1,
      cart: {},
      cart_by_id: {},
      cart_count: 0,
      cart_list: [],
      isLoading: {
        add_to_cart: false
      },
      claimed_food: {
        status: null
      }
    };
  },
  getters: {
    total_price: function total_price() {
      return this.valid_date_cart.reduce(function (total, item) {
        return total + Number(item.food_menu.price) * Number(item.quantity);
      }, 0);
    },
    valid_date_cart: function valid_date_cart(state) {
      return state.cart_list.filter(function (e) {
        return e.food_menu.is_date_valid;
      });
    }
  },
  actions: {
    getCart: function getCart(payload) {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee() {
        var _yield$axios$get, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this.claimed_food = {
                  status: null
                };
                _context.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default().get("api/cart/".concat(payload.instance));

              case 3:
                _yield$axios$get = _context.sent;
                data = _yield$axios$get.data;
                // get cart
                _this.cart = data;

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    claimFood: function claimFood(username) {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee2() {
        var _yield$axios$get2, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _this2.claimed_food = {
                  status: null
                };
                _context2.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default().get("api/cart/".concat(username, "/claim-food"));

              case 3:
                _yield$axios$get2 = _context2.sent;
                data = _yield$axios$get2.data;

                // get cart
                if (Object.keys(data).length) {
                  _this2.claimed_food = _objectSpread({}, data);
                  _this2.claimed_food.status = 'not-empty';
                } else {
                  _this2.claimed_food = _objectSpread({}, data);
                  _this2.claimed_food.status = 'empty';
                }

              case 6:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    getCartById: function getCartById(id) {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee3() {
        var _yield$axios$get3, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return axios__WEBPACK_IMPORTED_MODULE_1___default().get("api/cart/".concat(id, "/cart-id"));

              case 2:
                _yield$axios$get3 = _context3.sent;
                data = _yield$axios$get3.data;
                // get cart
                _this3.cart_by_id = data;

              case 5:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    addToCart: function addToCart(payload) {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee4() {
        var _yield$axios$get4, data, cart_food_menu;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _this4.isLoading.add_to_cart = true;
                _context4.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default().get("api/cart/".concat(payload.instance));

              case 3:
                _yield$axios$get4 = _context4.sent;
                data = _yield$axios$get4.data;
                // get cart
                _this4.cart = data;
                cart_food_menu = {
                  'cart_id': _this4.cart.id,
                  'menu_id': payload.menu_id,
                  'quantity': _this4.quantity
                };
                _context4.next = 9;
                return axios__WEBPACK_IMPORTED_MODULE_1___default().post('api/cart-food-menu', cart_food_menu);

              case 9:
                _context4.next = 11;
                return _this4.getFoodCart(payload.instance);

              case 11:
                _this4.isLoading.add_to_cart = false;

              case 12:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }))();
    },
    addQty: function addQty(cart, instance) {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee5() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return axios__WEBPACK_IMPORTED_MODULE_1___default().put("api/cart-food-menu/".concat(cart.id), cart);

              case 2:
                _context5.next = 4;
                return _this5.getFoodCart(instance);

              case 4:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }))();
    },
    // async countCart(instance) {
    //     const {data} = await axios.get(`api/cart/${instance}/count`); // get cart
    //     this.cart_count = data
    // },
    getFoodCart: function getFoodCart(instance) {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee6() {
        var _yield$axios$get5, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.next = 2;
                return axios__WEBPACK_IMPORTED_MODULE_1___default().get("api/cart-food-menu", {
                  params: {
                    instance: instance
                  }
                });

              case 2:
                _yield$axios$get5 = _context6.sent;
                data = _yield$axios$get5.data;
                // get cart
                _this6.cart_list = data;

              case 5:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }))();
    },
    // async deleteFoodMenu(id) {
    //     const {data} = await axios.delete(`api/cart-food-menu`, {
    //         params: {
    //             instance: instance
    //         }
    //     }); // get cart
    //
    //     const {data} = await axios.get(`api/cart/${payload.instance}`); // get cart
    //     this.cart_list = data
    // },
    deleteFoodMenu: function deleteFoodMenu(id) {
      var _this7 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee7() {
        var index;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                index = _this7.cart_list.findIndex(function (cart_list) {
                  return cart_list.id === id;
                });
                _context7.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default()["delete"]("api/cart-food-menu/".concat(id));

              case 3:
                _context7.next = 5;
                return _this7.cart_list.splice(index, 1);

              case 5:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7);
      }))();
    }
  }
});

/***/ }),

/***/ "./resources/js/store/VmcSubsidies.js":
/*!********************************************!*\
  !*** ./resources/js/store/VmcSubsidies.js ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "useVmcSubsidyStore": () => (/* binding */ useVmcSubsidyStore)
/* harmony export */ });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var pinia__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! pinia */ "./node_modules/pinia/dist/pinia.esm-browser.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



var useVmcSubsidyStore = (0,pinia__WEBPACK_IMPORTED_MODULE_2__.defineStore)("vmc_subsidy", {
  state: function state() {
    return {
      subsidy: {}
    };
  },
  getters: {},
  actions: {
    getSubsidy: function getSubsidy() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee() {
        var _yield$axios$get, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return axios__WEBPACK_IMPORTED_MODULE_1___default().get("api/vmc-subsidies/get-subsidy");

              case 2:
                _yield$axios$get = _context.sent;
                data = _yield$axios$get.data;
                _this.subsidy = data;

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    }
  }
});

/***/ }),

/***/ "./resources/js/pages/reservation/cartView.vue":
/*!*****************************************************!*\
  !*** ./resources/js/pages/reservation/cartView.vue ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _cartView_vue_vue_type_template_id_133bc49e__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./cartView.vue?vue&type=template&id=133bc49e */ "./resources/js/pages/reservation/cartView.vue?vue&type=template&id=133bc49e");
/* harmony import */ var _cartView_vue_vue_type_script_setup_true_lang_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./cartView.vue?vue&type=script&setup=true&lang=js */ "./resources/js/pages/reservation/cartView.vue?vue&type=script&setup=true&lang=js");
/* harmony import */ var E_laragon_www_vmcfrp_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,E_laragon_www_vmcfrp_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_cartView_vue_vue_type_script_setup_true_lang_js__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_cartView_vue_vue_type_template_id_133bc49e__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"resources/js/pages/reservation/cartView.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (__exports__);

/***/ }),

/***/ "./resources/js/pages/reservation/cartView.vue?vue&type=script&setup=true&lang=js":
/*!****************************************************************************************!*\
  !*** ./resources/js/pages/reservation/cartView.vue?vue&type=script&setup=true&lang=js ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_cartView_vue_vue_type_script_setup_true_lang_js__WEBPACK_IMPORTED_MODULE_0__["default"])
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_cartView_vue_vue_type_script_setup_true_lang_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./cartView.vue?vue&type=script&setup=true&lang=js */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/pages/reservation/cartView.vue?vue&type=script&setup=true&lang=js");
 

/***/ }),

/***/ "./resources/js/pages/reservation/cartView.vue?vue&type=template&id=133bc49e":
/*!***********************************************************************************!*\
  !*** ./resources/js/pages/reservation/cartView.vue?vue&type=template&id=133bc49e ***!
  \***********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_cartView_vue_vue_type_template_id_133bc49e__WEBPACK_IMPORTED_MODULE_0__.render)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_cartView_vue_vue_type_template_id_133bc49e__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./cartView.vue?vue&type=template&id=133bc49e */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/pages/reservation/cartView.vue?vue&type=template&id=133bc49e");


/***/ })

}]);