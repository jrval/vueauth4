<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use HasFactory;

    protected $connection = 'sqlsrv2';

    protected $guarded = [];

    protected $casts = [
        'created_at' => 'datetime:M d, Y',
        'is_claimed' => 'boolean',
        'claimed_qty' => 'integer',
        'price' => 'float',
        'quantity' => 'integer',

    ];

    protected $table = 'order_details';

    public function header()
    {
        return $this->belongsTo(OrderHeader::class, 'order_header_reference', 'id');
    }

    public function food_menu()
    {
        return $this->belongsTo(FoodMenu::class, 'food_menu_id', 'id');
    }
}
