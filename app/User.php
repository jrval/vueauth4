<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    protected $connection = 'sqlsrv';

    use HasRoles, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'datetime:M d, Y',
    ];

    public static function UserWithRoles($id)
    {
        $user = User::with(array('roles' => function ($query) {
            $query->select('name');
        }))->find($id);
        $map['id'] = $user->id;
        $map['name'] = $user->name;
        $map['username'] = $user->username;
        $map['email'] = $user->email;
        $map['roles'] = $user->roles->map(function ($user) {
            return $user->name;
        });

        return $map;
    }

    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    public function organic()
    {
        return $this->hasOne(Organic::class, 'eeno', 'username');
    }


}
