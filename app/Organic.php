<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Organic extends Model
{
    use HasFactory;

    protected $connection = 'sqlsrv';

    protected $table = 'organics';
}
