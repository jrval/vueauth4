<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CartFoodMenu extends Model
{
    use HasFactory, SoftDeletes;


    protected $connection = 'sqlsrv2';
    protected $table = 'cart_food_menus';
    protected $primaryKey = 'id';

    protected $guarded = [];
    protected $casts = [
        'quantity' => 'integer',
    ];


    public function food_menu()
    {
        return $this->belongsTo(FoodMenu::class, 'food_menu_id', 'id');
    }

}
