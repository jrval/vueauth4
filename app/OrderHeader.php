<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class OrderHeader extends Model
{
    use HasFactory;

    public $incrementing = false;
    protected $guarded = [];
    protected $connection = 'sqlsrv2';
    protected $casts = [
        'created_at' => 'datetime:M d, Y',
        'date_transaction' => 'datetime:m/d/Y',
        'subsidy' => 'float',
        'total' => 'float',
        'total_net' => 'float',
        'is_claimed' => 'boolean'
    ];
    protected $primaryKey = 'id';
    protected $table = 'order_header';
    protected $keyType = 'uuid';

    public function details()
    {
        return $this->hasMany(OrderDetail::class, 'order_header_reference', 'id');
    }


    public function users()
    {
        $database = Config::get('database.connections')['sqlsrv']['database'] . '.dbo.';
//        return $this->belongsTo(User::class, $database . 'username', 'username', '');
        return $this->hasOne(User::class, 'username', 'username');
    }
}
