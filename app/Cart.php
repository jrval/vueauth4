<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    public $incrementing = false;
    protected $connection = 'sqlsrv2';
    protected $table = 'carts';
    protected $primaryKey = 'id';

    protected $keyType = 'uuid';

    protected $guarded = [];


    public function foodMenus()
    {
        return $this->hasMany(CartFoodMenu::class, 'cart_id', 'id');
    }

    public function user()
    {

        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
