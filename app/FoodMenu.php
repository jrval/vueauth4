<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FoodMenu extends Model
{

    protected $connection = 'sqlsrv2';
    use HasFactory;

    protected $casts = [
        'created_at' => 'datetime:M d, Y',
        'image_details' => 'object',
        'date' => 'datetime:l, M d, Y',
        'is_date_valid' => 'boolean',
        'is_already_reserved' => 'boolean'
    ];
    protected $primaryKey = 'id';

    protected $appends = ['is_date_valid', 'is_already_reserved'];

    public function getIsAlreadyReservedAttribute()
    {
        $user_id = auth()->user()->id;
        $cart = optional(Cart::where('user_id', $user_id)->where('instance', 'adding')->first());
        return CartFoodMenu::withTrashed()->where('cart_id', $cart->id)->where('food_menu_id', $this->id)->exists();
    }

    public function getIsDateValidAttribute()
    {
        //$day = 'Friday';
//        $date = Carbon::parse($this->date)->format('Y-m-d');
//        $lastDate = Carbon::createFromTimeStamp(strtotime("last $day", $date));
        $date = Carbon::now()->timestamp;
        $lastDate = Carbon::parse($this->date)->subWeek()->startOfWeek()->next(Carbon::FRIDAY)->timestamp;
        return $date < $lastDate;

//        $date_today = Carbon::parse()->format('Y-m-d');
//
//        $date = Carbon::parse($this->date)->format('Y-m-d');
//
//        return $date >= $date_today;

    }

    public function scopeDateIsValid($query)
    {
        $date_today = Carbon::parse()->format('Y-m-d');
        $query->where('date', '>=', $date_today);
    }

}
