<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateFoodMenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'name' => 'required',
            'ingredients' => 'nullable|min:5|max:300',
            'allergens' => 'nullable|max:300',
            'price' => 'required|numeric|regex:/^\d+(\.\d{1,2})?$/|min:0.01',
            'date_selected' => 'date_format:Y-m-d|required|unique:sqlsrv2.food_menus,date,' . $this->id
        ];


        if ($this->hasFile('image_upload')) {
            $rules['image_upload'] = 'nullable:mimes:jpeg,jpg,png|max:50000';
        }
        return $rules;
    }


    public function attributes()
    {
        return [
            'image_upload' => 'image',
        ];
    }

    public function messages()
    {

        return [
            'date_selected.unique' => 'The selected date has already been added in the menu. Please select other dates',
        ];
    }

}
