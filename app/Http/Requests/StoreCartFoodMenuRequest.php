<?php

namespace App\Http\Requests;

use App\CartFoodMenu;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;

class StoreCartFoodMenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $database = Config::get('database.connections')['sqlsrv2']['database'] . '.dbo.';
        $food_id = $this->menu_id;
        $cart_id = $this->cart_id;

        return [
            'menu_id' => ['required', Rule::unique(CartFoodMenu::class, 'food_menu_id')
                ->where(function ($query) use ($food_id, $cart_id) {
                    return $query->where('cart_id', $cart_id)
                        ->where('food_menu_id', $food_id)->where('deleted_at', null);
                })]
        ];
    }

    public function attributes()
    {
        return [
            'menu_id' => 'food menu',
        ];
    }

    public function messages()
    {

        return [
            'menu_id.unique' => 'The selected food menu was already in the basket/reserved',
        ];
    }
}
