<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class StoreFoodMenuRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

//    public function validator($factory)
//    {
//        return $factory->make(
//            $this->sanitize(), $this->container->call([$this, 'rules']), $this->messages()
//        );
//    }
//
//    public function sanitize()
//    {
//        $date = Carbon::createFromFormat('D M d Y H:i:s e+', $this->date_selected);
//
//        $this->merge([
//            'date_selected' => $date->format('Y-m-d')
//        ]);
//        return $this->all();
//    }
    public function rules()
    {
        $rules =  [
            'name' => 'required',
            'ingredients' => 'nullable|min:5|max:300',
            'allergens' => 'nullable|max:300',
            'price' => 'required|numeric|regex:/^\d+(\.\d{1,2})?$/|min:0.01',
            'date_selected'=>'date_format:Y-m-d|required|unique:sqlsrv2.food_menus,date'
        ];


        if($this->hasFile('image_upload')){
            $rules['image_upload']='nullable:mimes:jpeg,jpg,png|max:50000';
        }
        return $rules;
    }


    public function attributes()
    {
        return [
            'image_upload' => 'image',
        ];
    }

    public function messages()
    {

        return [
            'date_selected.unique' => 'The selected date has already been added in the menu. Please select other dates',
        ];
    }
}
