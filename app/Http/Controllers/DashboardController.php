<?php

namespace App\Http\Controllers;

use App\FoodMenu;
use App\OrderDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function getTodaysFood()
    {

        $date_today = Carbon::now();

        $data = FoodMenu::whereDate('date', $date_today)->first();

        return response()->json($data);
    }

    public function countOrdered()
    {

        $date_today = Carbon::now();
        $food_today = optional(FoodMenu::whereDate('date', $date_today)->first());
        $data = OrderDetail::where('food_menu_id', $food_today->id)->count();

        return response()->json($data);
    }

    public function countClaimed()
    {

        $date_today = Carbon::now();
        $food_today = optional(FoodMenu::whereDate('date', $date_today)->first());
        $data = OrderDetail::where('food_menu_id', $food_today->id)->where('is_claimed', true)->count();

        return response()->json($data);
    }
}
