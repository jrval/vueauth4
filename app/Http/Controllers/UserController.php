<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function dataTable(Request $request): \Illuminate\Http\JsonResponse
    {

        abort_if(Gate::denies('user_view'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $searchValue = $request->search;
        $columnOrder = $request->sortby;
        $orderDirection = $request->sortdir;
        $perPage = intVal($request->currentpage);

        $query = User::with('roles')
            ->where('name', 'LIKE', "%$searchValue%")
            ->where('username', 'LIKE', "%$searchValue%")
            ->where('email', 'LIKE', "%$searchValue%")
            ->orderBy($columnOrder, $orderDirection)->paginate($perPage);

        return response()->json($query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        abort_if(Gate::denies('create_user'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255', 'unique:users', 'regex:/(^[A-Za-z0-9-_]+$)+/'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'roles' => ['required'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $user = User::create($request->all());
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->assignRole($roles);


        return response()->json($user);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        abort_if(Gate::denies('user_view'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $data = User::UserWithRoles($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        abort_if(Gate::denies('update_user') , Response::HTTP_FORBIDDEN, '403 Forbidden');


        $request->validate([
            'name' => 'required',
            'username' => 'required|string|max:25|unique:users,username,'.$id.'|regex:/(^[A-Za-z0-9-_]+$)+/',
            'email' => 'required|email|max:255|unique:users,email,' . $id,
            'password' => 'sometimes|confirmed',
            'password_confirmation' => 'required_with:password|same:password',
            'roles' => 'required',
        ]);

        $user = User::findOrFail($id);
        $user->update($request->except('roles', 'password_confirmation'));
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->syncRoles($roles);
        return  response()->json($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): \Illuminate\Http\JsonResponse
    {
        abort_if(Gate::denies('destroy_user') , Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user = User::findorfail($id);
        $user->delete();


        return response()->json($user);
    }
}
