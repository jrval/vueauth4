<?php

namespace App\Http\Controllers;

use App\Http\Resources\AbilityResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class AbilitiesController extends Controller
{
    public function index()
    {


        $roles = auth()->user()->getRoleNames();
        $permissions = auth()->user()->getPermissionsViaRoles()
            ->pluck('name');

        $abilities['roles'] = $roles;
        $abilities['permissions'] = $permissions;
        $abilities['user'] = auth()->user();
        return response()->json($abilities);

    }
}
