<?php

namespace App\Http\Controllers;

use App\VmcSubsidy;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VmcSubsidyController extends Controller
{
    public function getSubsidy()
    {


        $data = VmcSubsidy::whereDate('effectivity_date_from', '<=', Carbon::now()->format('Y-m-d'))
            ->whereDate('effectivity_date_to', '>=', Carbon::now()->format('Y-m-d'))->first();

        return response()->json($data);
    }
}
