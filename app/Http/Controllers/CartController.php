<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Http\Requests\StoreCartRequest;
use App\Http\Requests\UpdateCartRequest;
use App\OrderDetail;
use App\OrderHeader;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCartRequest $request
     * @return Response
     */
    public function store(StoreCartRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Cart $cart
     * @return JsonResponse
     */
    public function show($instance)
    {

        $auth = auth()->user()->id;
        $cart = Cart::firstOrCreate(
            ['user_id' => $auth],
            [
                'id' => Str::uuid(),
                'instance' => $instance
            ]
        );

        return response()->json($cart);
    }

    public function getCartById($cart)
    {
        if (Str::isUuid($cart)) {
            $data = Cart::with('user')->findorFail($cart);
            return response()->json($data);
        } else {
            abort(400);
        }
    }

    public function claimFood($username)
    {


        $data = DB::connection('sqlsrv2')->transaction(function () use ($username) {
            //get order header by username
            $headers = OrderHeader::where('username', $username)->where('is_claimed', false)->pluck('id');

            $date_today = Carbon::parse()->format('Y-m-d');

            $details = OrderDetail::with('food_menu')->
            whereHas('food_menu', function ($q) use ($date_today) {
                $q->whereDate('date', $date_today);
            })
                ->where(function ($q) use ($headers) {
                    $q->wherein('order_header_reference', $headers)
                        ->where('is_claimed', false);
//                        ->whereColumn('claimed_qty', '<', 'quantity');
                })->latest()
                ->first();

            if ($details) {

                $date_today_with_time = Carbon::parse()->format('Y-m-d H:i:s');
                $order_detail = OrderDetail::where('order_header_reference', $details->order_header_reference)->where('food_menu_id', $details->food_menu_id)
                    ->update([
                        'is_claimed' => true,
                        'date_claimed' => $date_today_with_time
                    ]);
                if ($order_detail) {
                    $count_detail = OrderDetail::where('order_header_reference', $details->order_header_reference)->where('is_claimed', false)->count();

                    if ($count_detail <= 0) {
                        $order_header = OrderHeader::find($details->order_header_reference);
                        $order_header->is_claimed = true;
                        $order_header->save();
                    }
                }


            }

            return $details;

        });

        return response()->json($data);
//        if (Str::isUuid($cart)) {
//            $data = Cart::with('user')->findorFail($cart);
//            return response()->json($data);
//        } else {
//            abort(400);
//        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCartRequest $request
     * @param Cart $cart
     * @return Response
     */
    public function update(UpdateCartRequest $request, Cart $cart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Cart $cart
     * @return Response
     */
    public function destroy(Cart $cart)
    {
        //
    }


    public function cartCount($instance)
    {

        $data = Cart::withCount('foodMenus')->where('instance', $instance)->where('user_id', auth()->user()->id)->first();

        return response()->json($data);
    }
}
