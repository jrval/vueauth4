<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): \Illuminate\Http\JsonResponse
    {
        $data = Role::all();
        return response()->json($data);
    }


    public function dataTable(Request $request): \Illuminate\Http\JsonResponse
    {
        abort_if(Gate::denies('read_role'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $searchValue = $request->search;
        $columnOrder = $request->sortby;
        $orderDirection = $request->sortdir;
        $perPage = intVal($request->currentpage);

        $data = Role::with('permissions')
            ->where('name', 'LIKE', "%$searchValue%")
            ->orderBy($columnOrder, $orderDirection)->paginate($perPage);

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        abort_if(Gate::denies('create_role'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $request->validate([
            'name' => 'required|unique:roles,name|regex:/(^[A-Za-z0-9-_]+$)+/',
            'permissions' => 'required'
        ]);

        $data = Role::create(['guard_name' => 'web', 'name' => $request->name]);
        $permissions = $request->input('permissions') ? $request->input('permissions') : [];
        $data->givePermissionTo($permissions);

        return response()->json($data);

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id): \Illuminate\Http\JsonResponse
    {
        $data = Role::RoleWithPermissions($id);
        return response()->json($data);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id): \Illuminate\Http\JsonResponse
    {

        abort_if(Gate::denies('update_role'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $request->validate([
            'name' => 'required|unique:roles,name,' . $request->role . '|regex:/(^[A-Za-z0-9-_]+$)+/',
            'permissions' => 'required'
        ]);


        $data = Role::findOrFail($id);
        $data->update($request->except('permissions'));
        $permissions = $request->input('permissions') ? $request->input('permissions') : [];
        $data->syncPermissions($permissions);
        return response()->json($data);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id): \Illuminate\Http\JsonResponse
    {
        abort_if(Gate::denies('destroy_role'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $data = Role::findorFail($id);
        $data->delete();

        return response()->json($data);
    }
}
