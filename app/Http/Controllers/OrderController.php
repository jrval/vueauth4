<?php

namespace App\Http\Controllers;

use App\CartFoodMenu;
use App\OrderDetail;
use App\OrderHeader;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;


class OrderController extends Controller
{
    public function storeOrder(Request $request)
    {

        //dd($request->valid_date_cart);
        //validation
//        CartFoodMenu::whereIn('cart_id')

        //insert order header

        $decode = json_decode($request->valid_date_cart);


        //loop food details to order details


        $data = DB::transaction(function () use ($request, $decode) {

            $order_header = OrderHeader::create([
                'id' => Str::uuid(),
                'username' => auth()->user()->username,
                'subsidy' => $request->subsidy_decimal,
                'total' => $request->total_price,
                'total_net' => $request->net_total,
                'date_transaction' => now(),
            ]);

            foreach ($decode as $food_menu) {

                OrderDetail::create([
                    'order_header_reference' => $order_header->id,
                    'food_menu_id' => $food_menu->food_menu_id,
                    'quantity' => $food_menu->quantity,
                    'price' => $food_menu->food_menu->price * $food_menu->quantity,
                ]);

                CartFoodMenu::where('food_menu_id', $food_menu->food_menu->id)->delete();

            }

            return true;
        });

        return response()->json($data);

    }

    public function dataTable(Request $request): JsonResponse
    {

        //abort_if(Gate::denies('read_permission'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $searchValue = $request->search;
        $columnOrder = $request->sortby;
        $orderDirection = $request->sortdir;
        $perPage = intVal($request->currentpage);

        $data = OrderHeader::with('details', 'details.food_menu')
            ->where('username', auth()->user()->username)
//            ->where('')
            ->latest()
            ->paginate($perPage);

        return response()->json($data);
    }

    public function orderListAll(Request $request): JsonResponse
    {


        abort_if(Gate::denies('order_main_view'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $searchValue = $request->search;
        $columnOrder = $request->sortby;
        $orderDirection = $request->sortdir;
        $perPage = intVal($request->currentpage);
        $startDate = Carbon::parse($request->start_date)->startOfDay()->format('Y-m-d H:i:s');
        $endDate = Carbon::parse($request->end_date)->endOfDay()->format('Y-m-d H:i:s');
        $query = OrderDetail::with(['header:id,subsidy,is_claimed,total,total_net,username', 'food_menu:id,date', 'header.users:id,username,name'])
            ->whereHas('food_menu', function ($q) use ($startDate, $endDate) {
                $q->whereBetween('date', [$startDate, $endDate]);
            })
            ->whereHas('header', function ($q) use ($searchValue) {
                $q->where('username', 'LIKE', "%$searchValue%");
            })
            ->where('is_claimed', $request->is_claimed)
            ->orderBy($columnOrder, $orderDirection)->paginate($perPage);
        return response()->json($query);
    }
}
