<?php

namespace App\Http\Controllers;

use App\FoodMenu;
use App\Http\Requests\StoreFoodMenuRequest;
use App\Http\Requests\UpdateFoodMenuRequest;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File as fileupload;
use Illuminate\Support\Facades\Storage;
use function response;


class FoodMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    public function filterByDate($start_date, $end_date): JsonResponse
    {


        $startDate = Carbon::parse($start_date)->startOfDay()->format('Y-m-d H:i:s');
        $endDate = Carbon::parse($end_date)->endOfDay()->format('Y-m-d H:i:s');

        $data = FoodMenu::where(function ($q) use ($startDate, $endDate) {
            $q->whereBetween('date', [$startDate, $endDate]);
        })->orderBy('date', 'asc')->get();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreFoodMenuRequest $request
     * @return JsonResponse
     */
    public function store(StoreFoodMenuRequest $request)
    {

        $date = Carbon::parse($request->date_selected);
        //$date = Carbon::createFromFormat('D M d Y H:i:s e+', $request->date_selected);

        $data = new FoodMenu();
        $data->name = $request->name;
        $data->ingredients = $request->ingredients;
        $data->allergens = $request->allergens;
        $data->price = $request->price;
        $data->date = $date->format('Y-m-d');


        if ($request->hasFile('image_upload')) {

            $data->image_details = $this->imageUpload($request);
        }

        $data->save();

        return response()->json($data);
    }

    public function imageUpload($request, $is_update = false, $image_details = null): object
    {

        if ($is_update) {

            $file_name = $image_details->filename ?? null;

            if ($file_name) {

                $exist = Storage::disk('local')->exists($file_name);

                if ($exist) {
                    Storage::delete($file_name);
                }
            }
        }


        $file = $request->file('image_upload');
        $destinationPath = 'files/menu';
        $newFilename = Storage::disk('local')->putFile($destinationPath, $file);

        return (object)[
            'orig_filename' => $file->getClientOriginalName(),
            'orig_ext' => $file->getClientOriginalExtension(),
            'size' => $file->getSize(),
            'mime_type' => $file->getClientMimeType(),
            'filename' => $newFilename,
            'ext' => $file->getClientOriginalExtension(),
        ];

    }

    /**
     * Display the specified resource.
     *
     * @param FoodMenu $foodMenu
     * @return JsonResponse
     */
    public function show(FoodMenu $foodMenu)
    {
        return response()->json($foodMenu);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFoodMenuRequest $request
     * @param FoodMenu $foodMenu
     * @return JsonResponse
     */
    public function update(UpdateFoodMenuRequest $request, FoodMenu $foodMenu)
    {

        $date = Carbon::parse($request->date_selected);

        $foodMenu->name = $request->name;
        $foodMenu->ingredients = $request->ingredients;
        $foodMenu->allergens = $request->allergens;
        $foodMenu->price = $request->price;
        $foodMenu->date = $date->format('Y-m-d');


        if ($request->hasFile('image_upload')) {

            $foodMenu->image_details = $this->imageUpload($request, true, $foodMenu->image_details);
        }

        $foodMenu->save();

        return response()->json($foodMenu);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param FoodMenu $foodMenu
     * @return JsonResponse
     */
    public function destroy(FoodMenu $foodMenu)
    {

        $file_name = $areaComponent->image_details->filename ?? null;
        if ($file_name) {
            $exist = Storage::disk('local')->exists($file_name);
            if ($exist) {
                Storage::delete($file_name);
            }
        }

        $foodMenu->delete();
        return response()->json($foodMenu);
    }
}
