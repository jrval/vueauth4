<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = Permission::orderBy('name','asc')->get();

        return response()->json($data);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function dataTable(Request $request): JsonResponse
    {

        abort_if(Gate::denies('read_permission'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $searchValue = $request->search;
        $columnOrder = $request->sortby;
        $orderDirection = $request->sortdir;
        $perPage = intVal($request->currentpage);

        $data = Permission::where('name', 'LIKE', "%$searchValue%")
            ->orderBy($columnOrder, $orderDirection)->paginate($perPage);

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        abort_if(Gate::denies('create_permission') , Response::HTTP_FORBIDDEN, '403 Forbidden');
        $request->validate([
            'name' => 'required|unique:permissions,name|regex:/(^[A-Za-z0-9-_]+$)+/',
        ]);

        $data = Permission::create(['guard_name' => 'web', 'name' => $request->name]);

        return response()->json($data);

    }


    public function show($id): JsonResponse
    {
        $data = Permission::findorFail($id);

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return JsonResponse
     */
    public function update(Request $request, $id): JsonResponse
    {
        abort_if(Gate::denies('update_permission') , Response::HTTP_FORBIDDEN, '403 Forbidden');
        $request->validate([
            'name' => 'required|unique:permissions,name,' . $request->permission . '|regex:/(^[A-Za-z0-9-_]+$)+/',
        ]);
        $data = Permission::find($id);
        $data->name = $request->name;
        $data->save();

        return response()->json($data);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        abort_if(Gate::denies('destroy_permission') , Response::HTTP_FORBIDDEN, '403 Forbidden');
        $data = Permission::findOrFail($id);
        $data->delete();
        return response()->json($data);
    }
}
