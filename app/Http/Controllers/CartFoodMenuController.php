<?php

namespace App\Http\Controllers;

use App\Cart;
use App\CartFoodMenu;
use App\Http\Requests\StoreCartFoodMenuRequest;
use App\Http\Requests\UpdateCartFoodMenuRequest;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CartFoodMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $cart = Cart::where('user_id', auth()->user()->id)->where('instance', $request->instance)->orderby('created_at', 'desc')->first();

        if ($cart) {
            $cart_list = CartFoodMenu::with('food_menu')->where('cart_id', $cart->id)->get();
            return response()->json($cart_list);
        }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCartFoodMenuRequest $request
     * @return JsonResponse
     */
    public function store(StoreCartFoodMenuRequest $request)
    {

        $data = CartFoodMenu::where('cart_id', $request->cart_id)
            ->where('food_menu_id', $request->menu_id)->first();

        if ($data) {
            $data->increment('quantity', $request->quantity);
        } else {
            $data = CartFoodMenu::create([
                'cart_id' => $request->cart_id,
                'food_menu_id' => $request->menu_id,
                'quantity' => $request->quantity
            ]);
        }

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param CartFoodMenu $cartFoodMenu
     * @return Response
     */
    public function show(CartFoodMenu $cartFoodMenu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param CartFoodMenu $cartFoodMenu
     * @return Response
     */
    public function edit(CartFoodMenu $cartFoodMenu)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCartFoodMenuRequest $request
     * @param CartFoodMenu $cartFoodMenu
     * @return JsonResponse
     */
    public function update(Request $request, CartFoodMenu $cartFoodMenu)
    {
        $request->validate([
            'quantity' => 'required|numeric|regex:/^\d+(\.\d{1,2})?$/|min:1',
        ]);
        $cartFoodMenu->quantity = $request->quantity;
        $cartFoodMenu->save();
        return response()->json($cartFoodMenu);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param CartFoodMenu $cartFoodMenu
     * @return JsonResponse
     */
    public function destroy(CartFoodMenu $cartFoodMenu)
    {
        $cartFoodMenu->delete();
        return response()->json($cartFoodMenu);
    }
}
