<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Permission extends \Spatie\Permission\Models\Permission
{
    use HasFactory;

    protected $casts = [
        //'created_at' => 'datetime:M d, Y',
        'created_at' => 'datetime:M d, Y',
    ];

}
