<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VmcSubsidy extends Model
{
    use HasFactory;

    protected $connection = 'sqlsrv2';


    protected $casts = [
        'created_at' => 'datetime:M d, Y',
    ];

    protected $table = 'vmc_subsidies';
}
