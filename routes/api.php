<?php

use App\Http\Controllers\AbilitiesController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CartFoodMenuController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FoodMenuController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VmcSubsidyController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//Auth::loginUsingId(1);
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {

    return $request->user();
});

Route::middleware('auth:sanctum')->group(function () {


    Route::get('abilities', [AbilitiesController::class, 'index']);


    Route::get('roles/datatable', [RoleController::class, 'dataTable']);
    Route::resource('roles', RoleController::class);


    Route::get('permissions/datatable', [PermissionController::class, 'dataTable']);
    Route::resource('permissions', PermissionController::class);


    Route::get('users/datatable', [UserController::class, 'dataTable']);
    Route::resource('users', UserController::class);


    Route::get('food-menu/{start_date}/{end_date}/filter-date', [FoodMenuController::class, 'filterByDate']);
    Route::resource('food-menu', FoodMenuController::class);


    Route::get('cart/{instance}/count', [CartController::class, 'cartCount']);
    Route::get('cart/{cart}/cart-id', [CartController::class, 'getCartById']);
    Route::get('cart/{username}/claim-food', [CartController::class, 'claimFood']);
    Route::resource('cart', CartController::class);


    Route::resource('cart-food-menu', CartFoodMenuController::class);


    Route::get('vmc-subsidies/get-subsidy', [VmcSubsidyController::class, 'getSubsidy']);


    Route::post('orders', [OrderController::class, 'storeOrder']);

    Route::get('orders/datatable', [OrderController::class, 'dataTable']);
    Route::get('orders/order-list-all', [OrderController::class, 'orderListAll']);

    Route::get('dashboard/todays-food', [DashboardController::class, 'getTodaysFood']);
    Route::get('dashboard/order-count', [DashboardController::class, 'countOrdered']);
    Route::get('dashboard/claimed-count', [DashboardController::class, 'countClaimed']);

});
