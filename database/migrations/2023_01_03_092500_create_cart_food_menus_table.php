<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartFoodMenusTable extends Migration
{
    protected $connection='sqlsrv2';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_food_menus', function (Blueprint $table) {
            $table->id();
            $table->uuid('cart_id');
            $table->bigInteger('food_menu_id')->index();
            $table->decimal('quantity',10,2,)->default(1)->index();
            $table->timestamps();


            $table->foreign('cart_id')->references('id')->on('carts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_food_menus');
    }
}
