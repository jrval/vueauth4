<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVmcSubsidiesTable extends Migration
{

    protected $connection = 'sqlsrv2';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vmc_subsidies', function (Blueprint $table) {
            $table->id();
            $table->integer('percentage')->nullable();
            $table->date('effectivity_date_from')->nullable();
            $table->date('effectivity_date_to')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vmc_subsidies');
    }
}
