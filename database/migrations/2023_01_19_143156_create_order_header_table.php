<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderHeaderTable extends Migration
{
    protected $connection = 'sqlsrv2';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_header', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->string('username')->index();
            $table->decimal('subsidy', 10, 2);
            $table->decimal('total', 10, 2);
            $table->decimal('total_net', 10, 2);
            $table->date('date_transaction');
            $table->boolean('is_claimed')->default(false);
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_header');
    }
}
