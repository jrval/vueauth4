<?php

namespace Database\Seeders;

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('spatie.permission.cache');


        Permission::create(['name' => 'permission_view']);
        Permission::create(['name' => 'permission_create']);
        Permission::create(['name' => 'permission_delete']);
        Permission::create(['name' => 'permission_edit']);
        Permission::create(['name' => 'role_view']);
        Permission::create(['name' => 'role_create']);
        Permission::create(['name' => 'role_delete']);
        Permission::create(['name' => 'role_edit']);
        Permission::create(['name' => 'user_create']);
        Permission::create(['name' => 'user_delete']);
        Permission::create(['name' => 'user_edit']);
        Permission::create(['name' => 'user_view']);
        Role::create(['name' => 'super_administrator']);
        $role = Role::create(['name' => 'administrator']);
        $role->givePermissionTo([
            'permission_view',
            'permission_create',
            'permission_delete',
            'permission_edit',
            'role_view',
            'role_create',
            'role_delete',
            'role_edit',
            'user_create',
            'user_delete',
            'user_edit',
            'user_view',
        ]);
//        //Create Super admin
        User::create([
            'name' => 'superadmin',
            'username' => 'sa',
            'email' => 'sa@test.com',
            'password' => Hash::make('password'),
        ]);
//
//        //Create Admin
        User::create([
            'name' => 'admin',
            'username' => 'admin',
            'email' => 'admin@test.com',
            'password' => Hash::make('password'),
        ]);

        $user = User::where('username', 'sa')->first();
//
        $user->assignRole('super_administrator');

        $user = User::where('username', 'admin')->first();

        $user->assignRole('administrator');
    }
}
